package org.hnau.scripts

data class RecombineInfo<I, O>(
        val sourceIndex: Int,
        val convert: (I) -> O
)

fun <I, O> List<I>.recombine(
        recombines: Iterable<RecombineInfo<I, O>>
) = recombines.map { (sourceIndex, convert) ->
    convert(this[sourceIndex])
}