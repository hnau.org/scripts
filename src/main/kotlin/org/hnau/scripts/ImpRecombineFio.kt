package org.hnau.scripts


object ImpRecombineFio {


    @JvmStatic
    fun main(args: Array<String>) {

        println(
                """
Olga E. Gorlova
Anastasia M. Kunshina, Marianna N. Litvinova
Kseniya M. Makhinya, Irina K. Fedorova
Alina R. Miniakhmetova, Natalia V. Shutemova
Anna S. Ponomareva, Nadezhda N. Menshakova
Alexandra S. Severyukhina, Maria V. Suvorova
Elizaveta I. Solovyova, Maria A. Khrustaleva

Alexandr S. Gertsen, Mariya A. Khruslaleva
Savelii S. Dushutin, Daria V. Perestoronina
Dmitry D. Kurushin, Yuriy N. Pinyagin
Anastasia A. Lagunova
Amaliya M. Mamedova, Yuriy N. Pinyagin
Dariya O. Remyannikova, Svetlana L. Mishlanova
Elizaveta Y. Shaad, Elizaveta Y. Shaad

Tatiana A. Gilmanova, Anastasia P. Chagina
Polina S. Osheva, Natalia V. Khorosheva
Olga V. Sannikova, Katerina Yu. Gladkova
Elizaveta D. Sapko, Tatyana N. Zubakina
Ksenia U. Ryumina, Timofey I. Starodumov, Anna M. Podgaets
Ivan M. Shumkov, Natalia V. Khorosheva

Oksana M. Altyntceva, Ekaterina N. Petkova
Lada V. Dolgikh, Daria V. Perestoronina
Tatyana A. Kazakova, Daria V. Perestoronina
Anna P. Markova, Yuriy N. Pinyagin

Ekaterina A. Kupriyanycheva
Vlada V. Iarkova, Maria A. Tsvetoukhina
Elizaveta I. Solovyova, Anna M. Podgaets
Marat R. Loikov
Igor A. Luchinin
Elizaveta I. Solovyova, Anna M. Podgaets

Anastasiia D. Chelpanova, Ekaterina L. Slovikova
Anastasia M. Balaburkina
Mariia A. Zalazaeva, Eugenia S. Yakovleva
Anton V. Kungurtsev, Larisa M. Alekseeva
Natalia N. Nikolina
Anastasiya A. Putina, Svetlana F. Plyasunova
Ivan A. Skorobrenko, Marina G. Zasedateleva
Maria I. Usanina, Svetlana F. Plyasunova
Vera S. Chiganaeva, Svetlana L. Mishlanova
Anastasiia D. Chelpanova, Ekaterina L. Slovikova

Veronica А. Annenkova, Olga I. Grafova
Ekaterina I. Bich, Nina S. Bochkareva
Elizaveta V. Buldakova, Nina S. Bochkareva
Elina S. Davletbaeva, Nina S. Bochkareva
Lyubov V. Krasilova
Andrey S. Sosonko
Ksenya E. Syrova
Yana O. Shebelbain 

Yuliya D. Bakhmatova, Olga I. Grafova
Olga D. Buneeva, Varvara A. Byachkova
Valeria A. Valeeva, Natalia A. Rudometova
Anna A. Gorislav, Varvara A. Byachkova
Alena S. Domorenko, Natalia E. Seibel
Elizaveta A. Korepanova, Inga V. Suslova
Svetlana S. Lekhanova, Lyudmila V. Bratukhina 

Dariia I. Kuldiurova, Inga V. Suslova
Elena S. Gondareva, Ekaterina V. Barinova
Olga S. Kapustina, Olga I. Grafova
Alla A. Ignateva, Alla P. Sklizkova 
Dariia I. Kuldiurova, Inga V. Suslova
Ganna A. Shirokova

Daniil A. Emtsev, Olga I. Grafova
Liudmila V. Kolobova, Katerina Yu. Gladkova
Victoria A. Nekrasova, Natalya V. Shutemova 
Iana A. Ogarkova, Olga I. Grafova
Maria V. Stupishina, Olga I. Grafova
                """
                        .trim()
                        .mapParts(
                                isPartSymbol = { char -> char !in setOf('\n', ',') }
                        ) { fio ->
                            fio
                                    .trim()
                                    .recombineFio(
                                            recombines = listOf(
                                                    RecombineFioInfo(
                                                            sourceIndex = 2,
                                                            short = false
                                                    ),
                                                    RecombineFioInfo(
                                                            sourceIndex = 0,
                                                            short = true
                                                    ),
                                                    RecombineFioInfo(
                                                            sourceIndex = 1,
                                                            short = true
                                                    )
                                            )
                                    )
                        }
                        .replace(",", ", ")
        )

    }

}