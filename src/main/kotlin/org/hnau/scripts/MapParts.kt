package org.hnau.scripts

import org.hnau.base.extensions.boolean.checkTruth
import org.hnau.base.extensions.ifNotEmpty
import org.hnau.base.extensions.number.ifPositive
import org.hnau.base.extensions.takeIfNotEmpty
import java.lang.StringBuilder


fun String.mapParts(
        isPartSymbol: (Char) -> Boolean,
        action: (String) -> String
): String {
    val result = StringBuilder()
    var part = StringBuilder()
    fun applyPart() {
        part.ifEmpty { return }
        val mappedPart = action(part.toString())
        result.append(mappedPart)
        part.clear()
    }
    forEach { char ->
        isPartSymbol(char).checkTruth(
                ifTrue = { part.append(char) },
                ifFalse = {
                    applyPart()
                    result.append(char)
                }
        )
    }
    applyPart()
    return result.toString()
}