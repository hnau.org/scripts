package org.hnau.scripts

import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.ifNotEmpty


private val wordsToSkip = hashSetOf("a", "an", "about", "as", "or", "from", "to", "for", "into", "according", "of", "and", "the", "in", "by", "on")

fun String.upFirst() = ifNotEmpty { string ->
    string.ifEmpty { return@ifNotEmpty "" }
    string[0].toUpperCase() + slice(1 until length)
} ?: ""

fun String.toEnCase() = toLowerCase().let { lower ->
    (lower in wordsToSkip).ifTrue { return@let lower }
    lower.upFirst()
}