package org.hnau.scripts

import org.hnau.base.extensions.boolean.checkTruth
import org.hnau.base.extensions.ifNotEmpty
import org.hnau.base.extensions.it

fun String.toShortFioPart() = ifNotEmpty { string ->
    string[0].toUpperCase() + "."
} ?: ""

data class RecombineFioInfo(
        val sourceIndex: Int,
        val short: Boolean = false
) {

    fun toRecombine() = RecombineInfo<String, String>(
            sourceIndex = sourceIndex,
            convert = short.checkTruth<(String) -> String>(
                    ifTrue = { String::toShortFioPart },
                    ifFalse = { ::it }
            )
    )

}

fun String.recombineFio(
        recombines: Iterable<RecombineFioInfo>,
        separator: String = " "
) = split(separator)
        .recombine(recombines.map(RecombineFioInfo::toRecombine))
        .joinToString(separator)